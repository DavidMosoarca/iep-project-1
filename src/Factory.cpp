#include "Factory.h"

Request* Request::createRequest(char* ip_sender, char* ip_receiver, char* received_data, string message) {
    return new Request(ip_sender, ip_receiver, received_data, message);
}