#include "Request.h"
#include "Package.h"
#include "Client.h"
#include "LoadBalancer.h"
#include "Server.h"
#include "DesktopClient.h"

using namespace std;

int main() {
    list<Request> internet_request_list = {};
    list<Package> internet_package_list = {};

    DesktopClient newClient("Samy", "192.168.0.2");
    DesktopClient c1(newClient);
    DesktopClient c2("David", "255.255.255.255");
    c2 = c2;

    // Item 13
    c1.sendRequest((char *)"192.168.0.2", (char *)"255.255.255.255", (char *)"Packages", "Hello my friend! How are you?");

    // Item 14
    Server server1("Samy Server");

    {
        cout << "Connect LoadBalancer_1 to Samy Server" << endl;
        LoadBalancer loadBalancer1(&server1);
    }

    {
        cout << "Connect LoadBalancer_2 to Samy Server" << endl;
        LoadBalancer loadBalancer2(&server1);
    }

    Server server2("David Server");
    LoadBalancer loadBalancer2(&server2);

    return 0;
}
