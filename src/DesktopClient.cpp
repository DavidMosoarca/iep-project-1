//
// Created by sam on 21.11.2020.
//

#include "DesktopClient.h"

DesktopClient::DesktopClient(string name, string ip)
: Client(name),
  ip(ip)
{
    cout << "DesktopClient constructor called" << endl;
    cout << "Name : " << name << "| IP : "<< ip << endl;
}

DesktopClient::~DesktopClient() {
    cout << "DesktopClient destroyed" << endl;
}

DesktopClient::DesktopClient(const DesktopClient& desktopClient)
: Client(desktopClient),
  ip(desktopClient.ip)
{
    cout << "DesktopClient copy-constructor" << endl;
}

DesktopClient& DesktopClient::operator=(DesktopClient desktopClient)
{
    Client::operator=(desktopClient);
    ip=desktopClient.ip;
    return *this;
}