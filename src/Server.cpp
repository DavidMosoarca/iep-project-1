#include <iostream>
#include "Server.h"
using namespace std;

Server::Server(string serverName)
:packages_on_server({}),
serverName(serverName),
 Server_Load(0)
{
    //this->Server_Load = 0;
    connectionStatus = false;
}

Server::~Server() {
    cout << "Server destroyed\n";
}

void Server::addPackageToServer(Package p) {
    packages_on_server.push_back(p);
}


int Server::getLoadServer() {
    return Server_Load;
}

void Server::setLoadServer(int new_packages, int old_packages) {
    this->Server_Load = Server_Load + new_packages - old_packages;
}

void Server::receivePackages() {
}

void Server::listPackagesOfServer() {
    for(Package p : packages_on_server) {
        cout << "The Request index of the package : " << p.getRequestIndex() << "\n";
        cout << "The Package index of the package : " << p.getPackageIndex() << "\n";
        cout << "The Request index of the package : " << p.getPackageData() << "\n";
    }
}