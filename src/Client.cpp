#include <iostream>
#include <bits/stdc++.h>
#include <algorithm>
#include "Request.h"
#include "Client.h"
#include <tr1/functional>
using namespace std;

Client::Client(string name)
: name(name)
{
    cout << "Client's name is: " << name << endl;
}

Client::~Client() {
}

Client::Client(const Client &client) {
    cout << "Client copy-constructor is called!" << endl;
}

Client& Client::operator=(const Client& client) {
    cout << "Client assignment-operator is called!" << endl;
    if(this == &client)
    //swap(client);
    return *this;
}

void Client::swap(Client& client) /*throw()*/ {
    using std::swap;
    swap(*this, client);
}

void Client::sendRequest(char* ip_sender, char* ip_receiver, char* received_data, string message) {
    /*
    char input[2049], ip_sender[18], ip_receiver[18];
    cout << "My Ip (Sender IP)\n";
    cin >> ip_sender;    cin.clear();
    cout << "My Ip is : " << ip_sender << "\n";
    cout << "Server Ip (Receiver IP)\n";
    cin >> ip_receiver;    cin.clear();
    cout << "Server's Ip is : " << ip_receiver << "\n";
    cin >> input;    cin.clear();
    Request newRequest(ip_sender, ip_receiver, input);
    return newRequest;
    */
    //std::auto_ptr<Request> pReq1(Request::createRequest(ip_sender, ip_receiver, received_data));
    shared_ptr<Request> pReq1(Request::createRequest(ip_sender, ip_receiver, received_data, message));

    cout << name << " send a request to IP: " << ip_receiver << " with message: " << endl;
    cout << "First Smart Pointer: " << pReq1->message << endl;

    shared_ptr<Request> pReq2(pReq1);
    cout << "Second Smart Pointer: " << pReq2->message << endl;
}