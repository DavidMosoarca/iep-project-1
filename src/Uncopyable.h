#ifndef IEP_PROJ_1___UNCOPYABLE_H
#define IEP_PROJ_1___UNCOPYABLE_H

class Uncopyable
{
protected:
    Uncopyable() {}
    ~Uncopyable() {}

private:
    Uncopyable(const Uncopyable&);
    Uncopyable& operator=(const Uncopyable&);
};

#endif
