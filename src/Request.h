#ifndef IEP_PROJ_1___REQUEST_H
#define IEP_PROJ_1___REQUEST_H

#include <iostream>
#include <list>
#include "Package.h"
using namespace std;

class Request {
private:
    char* ip_sender;
    char* ip_receiver;
    int request_index, total_number_of_packages_of_request;
    static int request_counter;
    char* received_data;
    list<Package> request_package_list;
public:
    static Request* createRequest(char* ip_sender, char* ip_receiver, char* received_data, string message);
    const list <Package> &getRequestPackageList() ;
    // ip_internal_server;
    // response_id;
    Request(char* ip_sender, char* ip_receiver, char* received_data, string message);
    ~Request();
    int getTotalNumberOfPackagesOfRequest();
    void setTotalNumberOfPackagesOfRequest();
    void splitRequestInPackages();
    int  getRequestIndex();
    void checkRequest();
    void getReceiverInternalIP();
    static int getRequestCounter();
    void listPackagesOfRequest();
    string message;
};

#endif
