//#include<iostream>
#include "LoadBalancer.h"
#include <list>
#include "Client.h"
#include "Request.h"
#include "Package.h"
using namespace std;

LoadBalancer::LoadBalancer(Server *s)
:server(s),
 internet_request_list({})
{
    bindServer(server);
    cout << server->serverName << " connected!" << endl;
}

LoadBalancer::~LoadBalancer()
{
    unbindServer(server);
    cout << server->serverName << " disconnected!" << endl;
}

void LoadBalancer::listRequest() {
    for(Request r : this->internet_request_list) {
        r.listPackagesOfRequest();
    }
}

void LoadBalancer::iterateReceivedRequestsList() {
    for(Request r : internet_request_list) {
        iteratePackagesList(r.getRequestPackageList());
    }
}

void LoadBalancer::iteratePackagesList(list<Package> listPackages) {
    for(Package p : listPackages) {
        sendPackageToServer(p);
    }
}

void LoadBalancer::addRequestToLoadBalancer(Request r) {
    internet_request_list.push_back(r);
}

void LoadBalancer::sendPackageToServer(Package p) {
    server->addPackageToServer(p);
}

void LoadBalancer::bindServer(Server *s) {
    while(s->connectionStatus) {}
    s->connectionStatus = true;
}

void LoadBalancer::unbindServer(Server *s) {
    s->connectionStatus = false;
}