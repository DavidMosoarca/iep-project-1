#ifndef IEP_PROJECT_1_FACTORY_H
#define IEP_PROJECT_1_FACTORY_H

#include "Client.h"
#include "Request.h"

class Factory {
public:
    Request* createRequest(char* ip_sender, char* ip_receiver, char* received_data);
};


#endif //IEP_PROJECT_1_FACTORY_H
