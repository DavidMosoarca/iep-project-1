#ifndef IEP_PROJECT_1_DESKTOPCLIENT_H
#define IEP_PROJECT_1_DESKTOPCLIENT_H

#include "Client.h"

class DesktopClient : public Client {
public:
    DesktopClient(string name, string ip);
    ~DesktopClient();
    DesktopClient(const DesktopClient&);
    DesktopClient& operator=(DesktopClient);
private:
    string ip;
};


#endif //IEP_PROJECT_1_DESKTOPCLIENT_H
