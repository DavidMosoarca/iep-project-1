#ifndef IEP_PROJ_1___CLIENT_H
#define IEP_PROJ_1___CLIENT_H

#include <iostream>
#include "Request.h"
#include "Package.h"

class Client {
public:
    Client(string name);
    ~Client();
    Client(const Client& client);
    Client& operator=(const Client& client);
    void sendRequest(char* ip_sender, char* ip_receiver, char* received_data, string message);
    void swap(Client& client);
private:
    string name;
    Request *req;
};

#endif
