#include <iostream>
#include <cstring>
#include "Request.h"
// #include "LoadBalancer.h"
using namespace std;

int Request::request_counter = 0;

Request::Request(char* ip_sender, char* ip_receiver, char* received_data, string message)
:request_index(request_counter),
 ip_sender(ip_sender),
 ip_receiver(ip_receiver),
 received_data(received_data),
 message(message),
 request_package_list({})
{
    this->request_counter++;
    //ip_sender[strlen(this->ip_sender)] = '\0';
    //ip_receiver[strlen(this->ip_receiver)] = '\0';
    //received_data[strlen(this->received_data)] = '\0';
    /*this->request_counter++;
    this->request_index = request_counter;
    strcpy(this->ip_sender, ip_sender);
    this->ip_sender[strlen(this->ip_sender)] = '\0';
    strcpy(this->ip_receiver, ip_receiver);
    this->ip_receiver[strlen(this->ip_receiver)] = '\0';
    strcpy(this->received_data, received_data);
    this->received_data[strlen(this->received_data)] = '\0';
    this->request_package_list = {};*/
}

Request::~Request()
{   cout << "Request destroyed\n";
}

int Request::getTotalNumberOfPackagesOfRequest() {
    int numberOfPackages = 0, received_data_length = strlen(this->received_data);
    numberOfPackages = received_data_length / 256;
    if(received_data_length % 256 != 0)
        numberOfPackages++;
    return numberOfPackages;
}

void Request::setTotalNumberOfPackagesOfRequest() {
    this->total_number_of_packages_of_request = this->getTotalNumberOfPackagesOfRequest();
}

// Determinam daca metoda apartine clasei Client
void Request::splitRequestInPackages() {
    int package_index = 0;                 //received_data.copy(segment, 256, 0);
    while(strlen(received_data) > 256) {
        char* segment = (char*)"";
        strcpy(segment, received_data);
        segment[256] = '\0';
        cout << segment << "\n";

        package_index++;
        Package new_package(segment, package_index, total_number_of_packages_of_request);
        request_package_list.push_back(new_package);

        char* aux_char = (char*)"";
        strcpy(aux_char, received_data+256);
        aux_char[strlen(aux_char)] = '\0';
        strcpy(received_data, aux_char);
        received_data[strlen(received_data)] = '\0';
    }
    if (strlen(received_data) > 0) {

        package_index++;
        Package lastPackage(received_data, package_index, total_number_of_packages_of_request);
        request_package_list.push_back(lastPackage);
    }
}

void Request::checkRequest() {
    if (request_package_list.empty())
        this->~Request();
    else
        /**/;
}

int Request::getRequestIndex() {
    return request_index;
}

int Request::getRequestCounter() {
    return request_counter;
}

void Request::listPackagesOfRequest() {
    for(Package p : this->request_package_list) {
        cout << "The Request index of the package : " << p.getRequestIndex() << "\n";
        cout << "The Package index of the package : " << p.getPackageIndex() << "\n";
        cout << "The Package data of the package : " << p.getPackageData() << "\n";
    }
}

// Determinam daca metoda apartine clasei LoadBalancer sau Request
void Request::getReceiverInternalIP() {
}

const list<Package> &Request::getRequestPackageList() {
    return request_package_list;
}
