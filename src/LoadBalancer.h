#ifndef IEP_PROJ_1___INTERNET_H
#define IEP_PROJ_1___INTERNET_H
#include "Request.h"
#include "Package.h"
#include "Uncopyable.h"
#include "Server.h"

class LoadBalancer: private Uncopyable {
private:
    Server *server;
    list<Request> internet_request_list;
    list<Package> internet_package_list;
public:
    LoadBalancer(Server *s);
    ~LoadBalancer();
    void addRequestToLoadBalancer(Request r);
    void listRequest();
    void iterateReceivedRequestsList();
    void iteratePackagesList(list<Package> listPackages);
    void sendPackageToServer(Package p);
    void bindServer(Server *s);
    void unbindServer(Server *s);
};

#endif
