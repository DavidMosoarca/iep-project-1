#ifndef IEP_PROJ_1___SERVER_H
#define IEP_PROJ_1___SERVER_H

#include <list>
#include "Package.h"
#include "Request.h"

class Server {
private:
    list<Package> packages_on_server;
    int Server_Load, packages_already_processed, new_packages_to_process;  // val_max = 1.000.000;
public:
    Server(string servername);
    ~Server();
    void addPackageToServer(Package p);
    int getLoadServer();
    void setLoadServer(int new_packages, int old_packages);
    void receivePackages();
    void listPackagesOfServer();
    bool connectionStatus;
    string serverName;
};

#endif
