#ifndef IEP_PROJ_1___PACKAGE_H
#define IEP_PROJ_1___PACKAGE_H

class Package {
private:
    char* package_data;
    int package_index, total_number_of_packages_of_request, request_index;
public:
    char *getPackageData();
    int getRequestIndex();
    int getPackageIndex();
    Package(char* package_data, int package_index, int total_number_of_packages_of_request);
    ~Package();
};

#endif
