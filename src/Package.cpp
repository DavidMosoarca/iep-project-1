//#include "Package.h"
#include<iostream>
#include "Package.h"
using namespace std;

Package::Package(char* package_data, int package_index, int total_number_of_packages_of_request)
:package_data(package_data),
 package_index(package_index),
 total_number_of_packages_of_request(total_number_of_packages_of_request)
{}

Package::~Package() {
}

int Package::getPackageIndex()  {
    return package_index;
}

char *Package::getPackageData()  {
    return package_data;
}

int Package::getRequestIndex()  {
    return request_index;
}
